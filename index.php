<?php
 use Controllers\Users;
 use Controllers\Photos;
 use Controllers\Home;

 
    $url = explode("/", trim($_SERVER["REQUEST_URI"]));
    $controllerName = $url[1];
    $methodName = $url[2];
    $params = [];
    $ClassName = 'controllers\\' . $controllerName;
      
    if($controllerName==""){
        echo "HELLO WORLD!";
    }
    
    for ($i = 3; $i < count($url); $i++) {
       if ($url[$i]) {
           $params[] = $url[$i];
       }
    }
    
    if (file_exists("controllers/".$controllerName.".php")) {
        spl_autoload_register(function ($Controller) {
            require_once($Controller.".php");
        }); 
    }
    
    if($controllerName != ""){
        if (class_exists($ClassName)) {
            $class = new $ClassName;
            if (method_exists($ClassName,$methodName)) {
                $class -> $methodName($params);
            
            } else if($methodName == '') {
                $class -> index();
            } else {
                echo "Class" . " " . $controllerName . " " . "does not have method of" . " " . $methodName . " " . "name";
            }
        } else {
            echo "Class" . " " . $controllerName . " " . "does not exist";
        }
    }
    
?>



